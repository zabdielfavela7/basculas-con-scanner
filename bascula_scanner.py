import sqlite3
from sqlite3 import Error
import multiprocessing
import paho.mqtt.publish as publish
import RPi.GPIO as GPIO
import os
import serial
from serial.tools import list_ports
import time as t
t.sleep(10)

import requests
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from subprocess import check_output

t.sleep(1)
print ("inicio")
ip=0
while ip==0:
	try:
		ip=int((str(((str(check_output(['hostname', '-I']))).split('.'))[3]))[0:2])
		#ip = (((str(check_output(['hostname', '-I']))).split('.'))[3])
		print ip
	except Error as r:
		print (r)

#configuracion de puertos de comunicacion
ports = list(list_ports.comports())
print (ports)
for x in list_ports.comports():
	print (x)
        if str(x[1])[0] == 'U':
            port2 = x[0]

ser = serial.Serial("/dev/ttyS0", 9600)
ser2 = serial.Serial(port2, 9600)

#VARIABLES CON EL NUMERO DEL GPIO EN DONDE ESTA CONECTADO EL RGB
red = 2
green = 22
blue = 3
GPIO.setmode(GPIO.BCM) 
GPIO.setup (red, GPIO.OUT)
GPIO.setup (green,GPIO.OUT)
GPIO.setup (blue,GPIO.OUT)

#-----------------------------INICIAMOS LAS VARIABLES QUE USAREMOS--------------
station =ip
worker = 0
repeticion=0 
cliente =str(ip)




###########################<<<<<FUNCIONES>>>>>########################

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#FUNCION PARPADEO DE LED VERDE PARA VERIFICAR QUE SE SUBIO LA CAJA
def verifica():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.HIGH)


def caja_subida():
        for x in range (0,10):
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                t.sleep(.1)
                GPIO.output(red, GPIO.LOW)
                GPIO.output(blue, GPIO.LOW)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.1)

def server_down():
        for x in range (0,6):
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(green, GPIO.LOW)
                GPIO.output(blue, GPIO.LOW)
                t.sleep(.05)
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.LOW)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.1)


def azul():
        GPIO.output(blue, GPIO.HIGH)
        GPIO.output(green, GPIO.LOW)
        GPIO.output(red, GPIO.LOW)

#FUNCION PARA CONTROLAR EL LED ROJO
def rojo():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.LOW)
        GPIO.output(blue, GPIO.LOW)

#FUNCION PARA CONTROLAR EL LED VERDE
def verde():
        GPIO.output(red, GPIO.LOW)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.LOW)

#FUNCION PARA HACER EL COLOR AMARILL0 EN EL RGB
def amarillo():
        GPIO.output(red, GPIO.HIGH)
        GPIO.output(green, GPIO.HIGH)
        GPIO.output(blue, GPIO.LOW)


#FUNCION PARA HACER LA VERIFICACION DE CAMBIO DE EMPLEADO
def magenta():
	for v in range (0,3):
                GPIO.output(red, GPIO.LOW)
                GPIO.output(blue, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.25) #PARPADEO DE .25 SEGUNDOS
                #PARPADEO EN COLOR BLANCO
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.15) #PARPADEO DE .15 SEGUNDOS

def ERROR_led():
        for v in range (0,3):
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.LOW)
                GPIO.output(green, GPIO.LOW)
                t.sleep(.25) #PARPADEO DE .25 SEGUNDOS
                #PARPADEO EN COLOR BLANCO
                GPIO.output(red, GPIO.HIGH)
                GPIO.output(blue, GPIO.HIGH)
                GPIO.output(green, GPIO.HIGH)
                t.sleep(.15) #PARPADEO DE .15 SEGUNDOS

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# FUNCION PARA CAMBIAR LA CAJA
def stop():
        print ("cambia de caja")
        finish=0
        while (finish == 0):
                t.sleep(.8)
                compare=PESO.value
                if compare>6:
                        pass
                if compare<=6:
                        y=datetime.now()
                        while (True):
                                compare2=PESO.value
                                if compare2>=6:
                                        y=datetime.now()
                                        break
                                if compare2<=6:
                                        dtt=datetime.now() - y
                                        time_lapse = (dtt.days * 24 *60 *60 +dtt.seconds)* 1000 +dtt.microseconds/1000.0
                                        if time_lapse>=500:
                                                print ("listo")
                                                finish=1
                                                break

#FUNCION PARA SUBIR LAS CAJAS MEDIANTE PROTOCOLO MQTT
def upload(peso):
        connn = sqlite3.connect('base_datos.db')
        #db = conn.cursor()
        broker="192.168.100.9"
        port=1883
        topic = "uploads"
        def on_publish(client,userdata,mid):
                print (mid)
                if mid==1:
                        print ("caja subida")
                        mid=0

        time = datetime.isoformat(datetime.utcnow())
        num = str(station) + time[5:7] + time[8:10] + time[11:13] + time[14:16] + time[17:19]
        weight=peso
        print (weight)
        time = (str((datetime.isoformat(datetime.utcnow()))[:-3])) + "Z"
        data = json.dumps({"id":int(num), "stationid": station, "time": time, "weight": weight})

        try:
                client1= mqtt.Client(cliente) #create client object
                client1.on_publish = on_publish #assign function to callback
                client1.connect(broker,port) #establish connection
                #print ("antes de enviar")
                client1.loop_start()
                ret= client1.publish(topic, data, 2) #publish
                client1.loop_stop()
                #print (ret)
                ret=str(ret)
                if ret=='(0, 1)':
                        db = connn.cursor()
                        db.execute("INSERT INTO alamcenamiento (status, message) values (?, ?)",(1, str(data)))
                        connn.commit()
                        connn.close()
                        #print ("guardado")
                        t.sleep (1)
        except:
                #print e
                #print ("ERRORRR")
                db = connn.cursor()
                db.execute("INSERT INTO alamcenamiento (status, message) values (?, ?)",(0, str(data)))
                connn.commit()
                connn.close()

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# FUNCION PARA COMPARAR EL PESO RECIBIDO CON LOS RANGOS Y SUBIR LA CAJA
def registro(PESO, condicion, pesoUnidad, pesoCaja, MxCaja, MnCaja, MxUnidad, MnUnidad):
	#conn = sqlite3.connect('base_datos.db')
	#db = conn.cursor()
	last_weight=0
            while True:
                    maxx=MxCaja.value
                    minn=MnCaja.value
                    ma=MxUnidad.value
                    mi=MnUnidad.value
                    cond=int(condicion.value)
                    if cond==1:
                            try:
                                    peso=PESO.value
                                    if (mi<=peso<=ma):
                                            pass
                                            #print (peso)
                                    if (minn<=peso<=maxx):
                                            #print (peso)
                                    if peso!=last_weight:
                                            print "tamos de vuelta"
                                            DONE.value=1
                                            upload(peso)
                                            last_weight=peso
                                            stop()
                                    else:
                                            print ("se repitio el peso")
                            except :
                                    pass
    				t.sleep(.01)
    		t.sleep(.01)
	t.sleep(.01)
############
############
def workeer():
	while True:
		try:
			if ser2.inWaiting()>1:
				worker = int(ser2.read(10))
				print (worker)
				son = json.dumps({"workerid": worker, "id":station})
				#"http://3.129.0.80:4000/b/worker"
				res = requests.put("http://3.129.0.80:4000/b/worker", data=son, headers={"Content-Type": "application/json"},timeout=2)
				#res = requests.put("http://192.168.100.11:4000/b/worker", data=son, headers={"Content-Type": "application/json"},timeout=2)
				print (res.status_code)
				if res.status_code == 200:
					print("se cambio de empleado a: " + str(worker))
					DONE.value=2
				else:
					DONE.value=3
					print("error no se pudo subir empleado")
		except:
    			try:
        				ports = list(list_ports.comports())
        				#print (ports)
        				for x in list_ports.comports():
        			        	#print (x)
        			        	if str(x[1])[0] == 'U':
        						port2 = x[0]
        				ser2 = serial.Serial(port2, 9600)
    			except:
    				pass
    			#print e
    			DONE.value=4
    			pass
    			t.sleep(.01)
		t.sleep(.01)
############
############
#FUNCION CONTROL DE BACKLIGTH
def RGB(MxUnidad, MnUnidad, MxCaja, MnCaja, PESO, DONE):
        while True:
                Max_uni=MxUnidad.value
                Min_uni=MnUnidad.value
                Max_caja=MxCaja.value
                Min_caja=MnCaja.value
                valor=PESO.value
                done=DONE.value

                if (done==0):
                        #--------- CONDICIONALES PARA INDICAR RANGO DE UNIDAD
                        if (valor<.10):
                                verifica()
                        if (.10<valor<Min_uni):
                                amarillo()
                        if (Min_uni<=valor<=Max_uni):
                                verde()
                        if (Max_uni<valor<=6):
                                rojo()
                        #----- CONDICIONALES PARA INDICAR RANGO DE CAJA
                        if (6<valor<Min_caja):
                                amarillo()
                        if (Min_caja<=valor<=Max_caja):
                                verde()
                        if (Max_caja<valor):
                                rojo()
                if (done==1):
                        caja_subida()
                        DONE.value=0
                if (done==2):
                        magenta()
                        DONE.value=0
                if (done==3):
                        ERROR_led()
                        DONE.value=0

                if (done==4):
                        server_down()
                        DONE.value=0

        		t.sleep(.01)
        t.sleep(.01)
############
############
#FUNCION PARA LEER EL PESO ENVIADO POR LA BASCULA
def bascula(PESO):
        pes=0.0
        ser.reset_input_buffer()
        while True:
                try:
                        if ser.inWaiting()>0:
                                if (ser.read()==b'S'):
                                        try:
                                                msg = ser.read_until(' ')
                                                peso_raw=msg[2:-1]
                                                condicion.value=1
                                                PESO.value=float(peso_raw)
                                        except:
                                                pass
                                if (ser.read()==b'D'):
                                        try:
                                                msg = ser.read_until(' ')
                                                peso_raw=msg[2:-1]
                                                condicion.value=2
                                                PESO.value=float(peso_raw)
                                        except:
                                                #print ("fallo conver")
                                                pass
                except:
                        #print ("falle en conversion")
                        ser.reset_input_buffer()
                        t.sleep(.01)
        		t.sleep(.01)
        t.sleep(.01)
############
############
# FUNCION PARA RANGOS DE LA CAJA
def rangos(MxCaja, MnCaja, MxUnidad, MnUnidad):
    while True:
        try:
                MQTT_SERVER = "192.168.100.9"
                MQTT_PATH = "rangos"
                def on_connect(client, userdata, flags, rc):
                        print("Connected with result code "+str(rc))
                        client.subscribe(MQTT_PATH)
                def on_message(client, userdata, msg):
                        mensaje=str(msg.payload)
                        #print (mensaje)
                        MxCaja.value = float(str((mensaje.split(','))[1]))
                        MnCaja.value = float(str((mensaje.split(','))[0]))
                        #print (MxCaja.value)
                        #print (MnCaja.value)
                        MxUnidad.value = float(str((mensaje.split(','))[3]))
                        MnUnidad.value = float(str((mensaje.split(','))[2]))
                        #print (MxUnidad.value)
                        #print (MnUnidad.value)
                client = mqtt.Client()
                client.on_connect = on_connect
                client.on_message = on_message
                client.connect(MQTT_SERVER, 1883, 60)
                client.loop_forever()
        except:
        		t.sleep(.01)
        		pass
        t.sleep(.01)

############
############
# FUNCION subir cajas de base de datos
def data_base_upload():
	waiting=10
	conn = sqlite3.connect('base_datos.db')
	#db = conn.cursor()
	broker="192.168.100.9"
	port=1883
	topic = "conexion"
	topic2= "uploads"
	while True:
		#db = conn.cursor()
        	flag=0
        	def on_publish(client,userdata,mid):
                	if mid==1:
                	        mid=0
        	while flag==0:
                	try:
                        	client1= mqtt.Client("cliente") #create client object
                        	client1.on_publish = on_publish #assign function to callback
                        	client1.connect(broker,port) #establish connection
                        	ret= client1.publish(topic, '.') #publish
                        	ret=str(ret)
                        	if ret=='(0, 1)':
                                	try:
                                            db = conn.cursor()
                                        	db.execute('SELECT * FROM alamcenamiento WHERE status = ?', (0, ))
                                        	value= db.fetchone()
                                        	#print value
                                        	conn.commit()
                                            conn.close()
                                            client1.loop_start()
                                        	ret= client1.publish(topic2, str(value[1]), 2) #publish
                                        	client1.loop_stop()
                                            ret=str(ret)
                                        	if ret=='(0, 2)':
                                        	        #print "envie"
                                                    db = conn.cursor()
                                                    db.execute('UPDATE alamcenamiento SET status = 1 WHERE message = ?', (str(value[1]), ))
                                                    conn.commit()
                                                    conn.close()
                                                    t.sleep (1)
                                    except:
                    						#conn.close()
                                            #print ("no hay mas datos perdidos")
                                            flag=1
                    except: #error de verificacion de conexion primario
				            #conn.close()
                        	#print ("error en la verificacion de comunicacion")
                        	flag=1
                    #conn.close()
                    #print "espera de 10 seg"
                    t.sleep(waiting)
#-------------------------------COMENZAMOS LOS PROGRAMAS THREAD----------------------------------------------------------------------------------------------


if __name__ == "__main__":

	try:
        	PESO = multiprocessing.Value('d', 0.0)
        	pesoCaja = multiprocessing.Value('d', 1000)
        	pesoUnidad = multiprocessing.Value('d', 100)
        	MxCaja = multiprocessing.Value('d', 20)
        	MnCaja = multiprocessing.Value('d', 17)
        	MxUnidad = multiprocessing.Value('d', 3.25)
        	MnUnidad = multiprocessing.Value('d', 3.150)
        	DONE = multiprocessing.Value('d', 0)
        	condicion = multiprocessing.Value('d', 0.0)

        	p1 = multiprocessing.Process(target=registro, args=(PESO, condicion, pesoUnidad, pesoCaja, MxCaja, MnCaja, MxUnidad, MnUnidad))
        	p2 = multiprocessing.Process(target=RGB, args=(MxUnidad, MnUnidad, MxCaja, MnCaja, PESO, DONE))
        	p3 = multiprocessing.Process(target=bascula, args=(PESO, ))
        	p4 = multiprocessing.Process(target=rangos, args=(MxCaja, MnCaja, MxUnidad, MnUnidad))
        	p5 = multiprocessing.Process(target=workeer, args=())
            p6 = multiprocessing.Process(target=data_base_upload, args=())


        	p1.start()
        	p2.start()
        	p3.start()
        	p4.start()
        	p5.start()
            p6.start()
	except KeyboardInterrupt:
    		print "Quit"
    		GPIO.cleanup()
